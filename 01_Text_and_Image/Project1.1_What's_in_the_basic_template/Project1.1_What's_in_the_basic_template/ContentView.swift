//
//  ContentView.swift
//  Project1.1_What's_in_the_basic_template
//
//  Created by XDLee on 2019/8/11.
//  Copyright © 2019 XDLee. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
