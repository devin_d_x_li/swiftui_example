//
//  ContentView.swift
//  Project1.3_How_to_style_text_views
//
//  Created by XDLee on 2019/8/15.
//  Copyright © 2019 XDLee. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
